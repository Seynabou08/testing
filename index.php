<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <title>PokemonGoBack</title>
</head>
<header>
    <div id="logo"></div>
</header>

<body>
    <div id="conf_board">
        <button id="config" value="config">config</button>
        <div id="conf_form">
            <p id="warning">Any not specified config will go default</p>
            <p>upload pokemon file
                <input type="file" name="pokemon_file"> </p>
            <p>upload abilityies file
                <input type="file" name="abilityies_file">
            </p>
            <p>upload deck file
                <input type="file" name="deck_file">
            </p>
            <p>set deck volume
                <input type="number" name="number_deck" value="60" max="100" min="30">
            </p>
        </div> 
         <button value="play" onclick="window.location.href='action/uploadAction.php'">play</button> 
    </div>
</body>
<script type="text/javascript">
$(document).ready(function() {
    $("#config").click(function() {
        $("#conf_form").toggle(400);
    }); 
});
 
</script>

</html>