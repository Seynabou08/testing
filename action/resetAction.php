<?php
include 'config_server.php';

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if (!$conn) { 
    die("Connection failed: " );
}   

$sql1 = "TRUNCATE TABLE PokemonCard"; 
$sql2 = "TRUNCATE TABLE EnergyCard"; 
$sql3 = "TRUNCATE TABLE TrainerCard"; 
$sql4 = "TRUNCATE TABLE Attacks"; 


$conn->query($sql1);
$conn->query($sql2);
$conn->query($sql3);
$conn->query($sql4);

$conn->close();
header('location:../game.php?reset=true'); 
exit();
 